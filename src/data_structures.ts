import { isEqual } from 'lodash';

export class ElementNode<T> {
    value: T;
    next: ElementNode<T> | null;

    constructor(element: T) {
        this.value = element;
        this.next = null;
    }
}

export class AbstractDataStructure<T> {
    rootNode: ElementNode<T> | null;
    length: number;

    constructor() {
        this.rootNode = null;
        this.length = 0;
    }

    print = () => {
        let cursor = this.rootNode;
        let position = 0;
        while(cursor !== null) {
            console.log(`Element:${position}:`, cursor.value, cursor.next);
            cursor = cursor.next;
            position++;
        }
    };

    size = () => {
        let cursor = this.rootNode;
        let counter = 0;
        while(cursor !== null) {
            cursor = cursor.next;
            counter++;
        }
        return counter;
    };

    isEmpty = () => {
        return this.rootNode === null;
    };

    search = (element: T) => {
        let cursor = this.rootNode;
        while(cursor !== null) {
            if(isEqual(cursor.value, element)) {
                return true;
            }
            cursor = cursor.next;
        }
        return false;
    };

    get = (index: number) => {
        if (index < 0) {
            return null;
        }
        let n = 0;
        let cursor = this.rootNode;
        while(cursor !== null && n < index) {
            cursor = cursor.next;
            n++;
        }
        return cursor;
    };

    replace = (index: number, element: T) => {
        if(index === 0) {
            const newElement = new ElementNode<T>(element);
            if(this.rootNode !== null) {
                newElement.next = this.rootNode.next;
            }
            this.rootNode = newElement;
            return this.rootNode;
        }

        const beforeElement = this.get(index-1);
        const cursor = beforeElement;
        if(cursor !== null) {
            const newElementNode = new ElementNode<T>(element);
            
            if(cursor.next !== null) {
                newElementNode.next = cursor.next.next;
                cursor.next = newElementNode;
                return newElementNode;
            }
        }
        return null;
    };

    toArray = () => {
        let cursor = this.rootNode;
        const toArray: T[] = [];
        while(cursor !== null) {
            toArray.push(cursor.value);
            cursor = cursor.next;
        }
        return toArray;
    };
}

// Stack data structure
// LIFO: Last in first out
export class Lifo<T> extends AbstractDataStructure<T>{
    constructor() {
        super();
    }

    push = (element: T) => {
        const newFirstElementNode = new ElementNode<T>(element);
        if (this.rootNode !== null) {
            newFirstElementNode.next = this.rootNode;
        }
        this.rootNode = newFirstElementNode;
        this.length++;
    };

    pop = () => {
        if (this.rootNode !== null) {
            const cursor = this.rootNode;
            this.rootNode = this.rootNode.next;
            this.length--;
            return cursor;
        }
    };
}

// Queue data structure
// FIFO: First in first out
export class Fifo<T> extends AbstractDataStructure<T>{
    tailNode: ElementNode<T> | null;

    constructor() {
        super();
        this.tailNode = null;
    }

    add = (element: T) => {
        const newElement = new ElementNode<T>(element);
        if (this.tailNode === null) {
            this.tailNode = newElement;
        } else {
            this.tailNode.next = newElement;
            this.tailNode = newElement;
        }
        if (this.rootNode === null) {
            this.rootNode = this.tailNode;
        }
        this.length++;
    };

    remove = () => {
        if (this.rootNode !== null) {
            const cursor = this.rootNode;
            this.rootNode = this.rootNode.next;
            this.length--;
            return cursor;
        }
    };
}