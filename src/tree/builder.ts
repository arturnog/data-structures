import {TreeNode} from './tree';
import {map, random} from 'lodash';

const generateRandString = (): string => {
    return Math.random().toString(36).substring(2, 15);
};

const generateRandNumber = (min = 0, max = 50): number => {
    return random(min, max);
};

export const buildTree = (rootPath = '', depth = 4, maxNumberOfChildrenPerLevel = 10, currentDepth = 0): TreeNode<string> => {
    return {
        path: rootPath,
        value: generateRandString(),
        children: !depth 
        ? [] 
        : map(Array(generateRandNumber(0, maxNumberOfChildrenPerLevel)), (_i) => buildTree(`${generateRandString()}_level${currentDepth+1}`, depth-1, maxNumberOfChildrenPerLevel, currentDepth+1))
    };
};