import {Lifo, Fifo} from './data_structures';
import { random } from 'lodash';

describe('LIFO stack', () => {
    it('test push operation', () => {
        const lifo = new Lifo<string>();
        const element1 = 'test1';
        const element2 = 'test2';

        lifo.push(element1);
        lifo.push(element2);

        expect(lifo.rootNode?.value).toEqual(element2);
        expect(lifo.rootNode?.next?.value).toEqual(element1);
    });

    it('test pop operation on empty stack', () => {
        const lifo = new Lifo<string>();
        
        const element = lifo.pop();

        expect(element).toBeUndefined();
        expect(lifo.rootNode).toBeNull();
    });

    it('test pop operation on stack', () => {
        const lifo = new Lifo<string>();
        const element1 = 'test1';
        const element2 = 'test2';
        
        lifo.push(element1);
        lifo.push(element2);

        const popElement = lifo.pop();

        expect(popElement?.value).toBe(element2);
        expect(popElement?.next?.value).toBe(element1);
        expect(lifo.rootNode?.value).toBe(element1);
        expect(lifo.rootNode?.next).toBeNull();
    });

    it('test size operation', () => {
        const lifo = new Lifo<string>();
        
        expect(lifo.size()).toBe(0);
        lifo.push('some-element');
        expect(lifo.size()).toBe(1);
        lifo.push('some-element');
        expect(lifo.size()).toBe(2);
        lifo.pop();
        expect(lifo.size()).toBe(1);
        lifo.push('some-element');
        expect(lifo.size()).toBe(2);
        lifo.pop();
        expect(lifo.size()).toBe(1);
        lifo.pop();
        expect(lifo.size()).toBe(0);
        lifo.pop();
        expect(lifo.size()).toBe(0);
        lifo.push('some-element');
        expect(lifo.size()).toBe(1);
        lifo.pop();
        expect(lifo.size()).toBe(0);
    });

    it('test isEmpty operation', () => {
        const lifo = new Lifo<string>();
        
        expect(lifo.isEmpty()).toBe(true);
        lifo.push('some-element');
        expect(lifo.isEmpty()).toBe(false);
        lifo.push('some-element');
        expect(lifo.isEmpty()).toBe(false);
        lifo.pop();
        expect(lifo.isEmpty()).toBe(false);
        lifo.push('some-element');
        expect(lifo.isEmpty()).toBe(false);
        lifo.pop();
        expect(lifo.isEmpty()).toBe(false);
        lifo.pop();
        expect(lifo.isEmpty()).toBe(true);
        lifo.pop();
        expect(lifo.isEmpty()).toBe(true);
        lifo.push('some-element');
        expect(lifo.isEmpty()).toBe(false);
        lifo.pop();
        expect(lifo.isEmpty()).toBe(true);
    });

    it('test search operation (found in middle of stack)', () => {
        const lifo = new Lifo<string>();
        const element = 'some-element';
        
        lifo.push('some-random-element');
        lifo.push(element);
        lifo.push('another-random-element');
        lifo.push('last-element-to-be-added');

        const searchResult = lifo.search(element);

        expect(searchResult).toBe(true);
    });

    it('test search operation (found at first position of stack)', () => {
        const lifo = new Lifo<string>();
        const element = 'some-element';
        
        lifo.push('some-random-element');
        lifo.push('another-random-element');
        lifo.push('last-element-to-be-added');
        lifo.push(element);

        const searchResult = lifo.search(element);

        expect(searchResult).toBe(true);
    });

    it('test search operation (found at last position of stack)', () => {
        const lifo = new Lifo<string>();
        const element = 'some-element';
        
        lifo.push(element);
        lifo.push('some-random-element');
        lifo.push('another-random-element');
        lifo.push('last-element-to-be-added');

        const searchResult = lifo.search(element);

        expect(searchResult).toBe(true);
    });

    it('test search operation (not found on empty stack)', () => {
        const lifo = new Lifo<string>();
        const element = 'some-element';

        const searchResult = lifo.search(element);

        expect(searchResult).toBe(false);
    });

    it('test search operation (not found on stack)', () => {
        const lifo = new Lifo<string>();
        const element = 'some-element';
        
        lifo.push('some-random-element');
        lifo.push('another-random-element');
        lifo.push('last-element-to-be-added');

        const searchResult = lifo.search(element);

        expect(searchResult).toBe(false);
    });

    it('test get operation (first position)', () => {
        const lifo = new Lifo<string>();
        const element = 'some-element';

        lifo.push(element);

        const getItemResult = lifo.get(0);

        expect(getItemResult?.value).toEqual(element);
        expect(getItemResult?.next).toBeNull();
    });

    it('test get operation (middle position)', () => {
        const lifo = new Lifo<string>();
        const element = 'some-element';
        const nextElement = 'inserted-before-tested-element';

        lifo.push('some-fist-element-to-add');
        lifo.push(nextElement);
        lifo.push(element);
        lifo.push('another');
        lifo.push('and-the-last-one');

        const getItemResult = lifo.get(2);

        expect(getItemResult?.value).toEqual(element);
        expect(getItemResult?.next?.value).toEqual(nextElement);
    });

    it('test get operation (last position)', () => {
        const lifo = new Lifo<string>();
        const element = 'some-element';
        
        lifo.push(element);
        lifo.push('some-second-element-to-add');
        lifo.push('another');
        lifo.push('and-the-last-one');
        
        const getItemResult = lifo.get(3);

        expect(getItemResult?.value).toEqual(element);
        expect(getItemResult?.next).toBeNull();
    });

    it('test get operation (empty stack)', () => {
        const lifo = new Lifo<string>();
        
        const getItemResult = lifo.get(0);

        expect(getItemResult).toBeNull();
    });

    it('test get operation (index over stack size)', () => {
        const lifo = new Lifo<string>();
        
        lifo.push('only-this-element-to-be-size-1');

        const getItemResult = lifo.get(1);

        expect(getItemResult).toBeNull();
    });

    it('test get operation (negative index)', () => {
        const lifo = new Lifo<string>();
        
        lifo.push('some-element');
        lifo.push('some-other-element');

        const getItemResult = lifo.get(-1);

        expect(getItemResult).toBeNull();
    });

    it('test toArray operation (empty stack)', () => {
        const lifo = new Lifo<string>();
        
        const lifoArray = lifo.toArray();

        expect(lifoArray).toEqual([]);
    });

    it('test toArray operation (stack with a single value)', () => {
        const lifo = new Lifo<string>();
        const singleElement = 'this-is-only-one-element';
        lifo.push(singleElement);

        const lifoArray = lifo.toArray();

        expect(lifoArray).toEqual([singleElement]);
    });

    it('test toArray operation (stack with multiple values)', () => {
        const lifo = new Lifo<number>();
        const elementsArray: number[] = [1,2,3,4,5,10,50,100,500,1000,-1];
        elementsArray.forEach(element => {
            lifo.push(element);
        });

        const lifoArray = lifo.toArray();

        expect(lifoArray).toEqual(elementsArray.reverse());
    });

    it('test add and size operation (add n times)', () => {
        const lifo = new Lifo<string>();
        const expectedSize = 15;

        for(let i=0;i<expectedSize;i++) {
            lifo.push(`element${i}`);
        }

        expect(lifo?.size()).toBe(expectedSize);
        expect(lifo.rootNode?.value).toBe(`element${expectedSize-1}`);
        expect(lifo.rootNode?.next?.value).toBe(`element${expectedSize-2}`);
    });

    it('test replace operation (valid index - stack with one element)', () => {
        const lifo = new Lifo<number>();
        const replacePos = 0;
        const replaceElement = 15;

        lifo.push(1);

        const replacedElement = lifo.replace(replacePos, replaceElement);
        
        const getReplacedElement = lifo.get(replacePos);

        expect(replacedElement.value).toEqual(replaceElement);
        expect(replacedElement.next).toEqual(null);
        expect(getReplacedElement.value).toEqual(replaceElement);
        expect(getReplacedElement.next).toEqual(null);
    });

    it('test replace operation (valid index - middle position)', () => {
        const lifo = new Lifo<number>();
        const replacePos = 1;
        const replaceElement = 15;
        const firstElement = 1;

        lifo.push(firstElement);
        lifo.push(10);
        lifo.push(100);

        const replacedElement = lifo.replace(replacePos, replaceElement);
        
        const getReplacedElement = lifo.get(replacePos);

        expect(replacedElement.value).toEqual(replaceElement);
        expect(replacedElement.next.value).toEqual(firstElement);
        expect(getReplacedElement.value).toEqual(replaceElement);
        expect(getReplacedElement.next.value).toEqual(firstElement);
    });

    it('test replace operation (valid index - last position)', () => {
        const lifo = new Lifo<number>();
        const replacePos = 2;
        const replaceElement = 15;
        const secondLastElementToAdd = 10;
        const lastElementToAdd = 100;

        lifo.push(1);
        lifo.push(secondLastElementToAdd);
        lifo.push(lastElementToAdd);

        const replacedElement = lifo.replace(replacePos, replaceElement);
        
        const getReplacedElement = lifo.get(replacePos);
        const getLastElementAdded = lifo.get(0);
        const getSecondLastElementAdded = lifo.get(1);

        expect(replacedElement.value).toEqual(replaceElement);
        expect(replacedElement.next).toEqual(null);
        expect(getReplacedElement.value).toEqual(replaceElement);
        expect(getReplacedElement.next).toEqual(null);
        expect(getSecondLastElementAdded.value).toEqual(secondLastElementToAdd);
        expect(getLastElementAdded.value).toEqual(lastElementToAdd);
    });

    it('test replace operation (valid index - first position)', () => {
        const lifo = new Lifo<number>();
        const replacePos = 0;
        const replaceElement = 15;
        const firstElementToAdd = 1;
        const secondElementToAdd = 10;

        lifo.push(firstElementToAdd);
        lifo.push(secondElementToAdd);
        lifo.push(100);

        const replacedElement = lifo.replace(replacePos, replaceElement);
        
        const getReplacedElement = lifo.get(replacePos);
        const getFirstElementAdded = lifo.get(2);
        const getSecondElementAdded = lifo.get(1);

        expect(replacedElement.value).toEqual(replaceElement);
        expect(replacedElement.next.value).toEqual(secondElementToAdd);
        expect(getReplacedElement.value).toEqual(replaceElement);
        expect(getReplacedElement.next.value).toEqual(secondElementToAdd);
        expect(getFirstElementAdded.value).toEqual(firstElementToAdd);
        expect(getSecondElementAdded.value).toEqual(secondElementToAdd);
    });

    it('test replace operation (invalid index - negative)', () => {
        const lifo = new Lifo<number>();
        const replacePos = -1;
        const replaceElement = 15;

        lifo.push(1);
        lifo.push(10);
        lifo.push(100);

        const replacedElement = lifo.replace(replacePos, replaceElement);
        
        const getReplacedElement = lifo.get(replacePos);

        expect(replacedElement).toEqual(null);
        expect(getReplacedElement).toEqual(null);
    });

    it('test replace operation (invalid index - over stack limit)', () => {
        const lifo = new Lifo<number>();
        const replacePos = 10;
        const replaceElement = 15;

        lifo.push(1);
        lifo.push(10);
        lifo.push(100);

        const replacedElement = lifo.replace(replacePos, replaceElement);
        
        const getReplacedElement = lifo.get(replacePos);

        expect(replacedElement).toEqual(null);
        expect(getReplacedElement).toEqual(null);
    });

    it('test length property (add/remove n times)', () => {
        const lifo = new Lifo<string>();
        let expectedSize = 15;
        
        expect(lifo.length).toBe(0);

        for(let i=0;i<expectedSize;i++) {
            expect(lifo.length).toBe(i);
            lifo.push(`element${i}`);
        }

        expect(lifo?.size()).toBe(expectedSize);
        expect(lifo.length).toBe(expectedSize);

        while(expectedSize > 0) {
            expect(lifo.length).toBe(expectedSize);
            lifo.pop();
            expectedSize--;
        }

        expect(expectedSize).toBe(0);
        expect(lifo?.size()).toBe(expectedSize);
        expect(lifo.length).toBe(expectedSize);
    });

    it('test length property (cannot be negative)', () => {
        const lifo = new Lifo<string>();
        
        expect(lifo.length).toBe(0);
        lifo.pop();
        expect(lifo.length).toBe(0);
        lifo.push('someElement');
        expect(lifo.length).toBe(1);
        lifo.pop();
        expect(lifo.length).toBe(0);
        lifo.pop();
        expect(lifo.length).toBe(0);
        lifo.pop();
        expect(lifo.length).toBe(0);
    });
});

describe('FIFO queue', () => {
    it('test add operation', () => {
        const fifo = new Fifo<string>();
        const element1 = 'test1';
        const element2 = 'test2';

        fifo.add(element1);
        fifo.add(element2);

        expect(fifo.rootNode?.value).toEqual(element1);
        expect(fifo.rootNode?.next?.value).toEqual(element2);
        expect(fifo.tailNode.value).toEqual(element2);
        expect(fifo.tailNode.next).toBeNull();
    });

    it('test remove operation (empty queue)', () => {
        const fifo = new Fifo<string>();

        const removedElement = fifo.remove();

        expect(removedElement).toBeUndefined();
        expect(fifo.rootNode).toBeNull();
    });

    it('test remove operation (only one element in queue)', () => {
        const fifo = new Fifo<string>();
        const element = 'this-is-the-one';
        
        fifo.add(element);
        const removedElement = fifo.remove();

        expect(removedElement?.value).toBe(element);
        expect(removedElement?.next).toBeNull();
        expect(fifo.rootNode).toBeNull();
    });

    it('test remove operation (on queue with content)', () => {
        const fifo = new Fifo<string>();
        const element = 'this-is-the-one';
        const nextElement = 'next-inserted-element';
        
        fifo.add(element);
        fifo.add(nextElement);
        const removedElement = fifo.remove();

        expect(removedElement?.value).toBe(element);
        expect(removedElement?.next?.value).toBe(nextElement);
        expect(fifo.rootNode?.value).toBe(nextElement);
        expect(fifo.rootNode?.next).toBeNull();
    });

    it('test search operation (with object comparison)', () => {
        interface TestObject {
            value: string;
            cost: number;
        };

        const fifo = new Fifo<TestObject>();
        
        const element: TestObject = {
            value: 'some-element',
            cost: 12345
        };
        
        const nElements = 20;
        const middlePos = nElements/2;
        for(let i=0; i<nElements; i++) {
            if(i === middlePos) {
                fifo.add(element);
            } else {
                fifo.add({
                    value: `element${i}`,
                    cost: random()
                });
            }
        }

        const searchResult = fifo.search(element);

        expect(searchResult).toBe(true);
    });

    it('test toArray operation (queue with multiple values)', () => {
        const fifo = new Fifo<string>();
        const elementsToAdd: string[] = ['this-is-one-element', 'and another', 'this 1', ''];
        elementsToAdd.forEach(element => {
            fifo.add(element);
        });

        const fifoArray = fifo.toArray();

        expect(fifoArray).toEqual(elementsToAdd);
    });

    it('test add and size operation (add n times)', () => {
        const fifo = new Fifo<string>();
        const expectedSize = 15;

        for(let i=0;i<expectedSize;i++) {
            fifo.add(`element${i}`);
        }

        expect(fifo?.size()).toBe(expectedSize);
        expect(fifo.rootNode?.value).toBe(`element0`);
        expect(fifo.rootNode?.next?.value).toBe(`element1`);
        expect(fifo.tailNode.value).toEqual(`element${expectedSize-1}`);
        expect(fifo.tailNode.next).toBeNull();
    });

    it('test length property (add/remove n times)', () => {
        const fifo = new Fifo<string>();
        let expectedSize = 15;
        
        expect(fifo.length).toBe(0);

        for(let i=0;i<expectedSize;i++) {
            expect(fifo.length).toBe(i);
            fifo.add(`element${i}`);
        }

        expect(fifo?.size()).toBe(expectedSize);
        expect(fifo.length).toBe(expectedSize);

        while(expectedSize > 0) {
            expect(fifo.length).toBe(expectedSize);
            fifo.remove();
            expectedSize--;
        }

        expect(expectedSize).toBe(0);
        expect(fifo?.size()).toBe(expectedSize);
        expect(fifo.length).toBe(expectedSize);
    });

    it('test length property (cannot be negative)', () => {
        const fifo = new Fifo<string>();
        
        expect(fifo.length).toBe(0);
        fifo.remove();
        expect(fifo.length).toBe(0);
        fifo.add('someElement');
        expect(fifo.length).toBe(1);
        fifo.remove();
        expect(fifo.length).toBe(0);
        fifo.remove();
        expect(fifo.length).toBe(0);
        fifo.remove();
        expect(fifo.length).toBe(0);
    });
});