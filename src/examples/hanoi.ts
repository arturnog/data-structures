import {Lifo} from '../data_structures';

export class Hanoi {
    numberOfDisks: number;
    readonly minMoves: number;
    initialTower: number[];
    auxiliarTower: number[];
    finalTower: number[];
    totalMoves: number;
    // not used
    stack: Lifo<number>;

    constructor(numberOfDisks = 6) {
        this.numberOfDisks = numberOfDisks;
        this.minMoves = Math.pow(2, this.numberOfDisks)-1;
        this.initialTower = this.createDefaultTower();
        this.auxiliarTower = [];
        this.finalTower = [];
        this.totalMoves = 0;
        this.stack = new Lifo();
        console.log("Number of disks:", this.numberOfDisks);
        console.log("Minimum moves possible:", this.minMoves);
    }

    solve = (numberOfDisks: number = this.numberOfDisks, initialTower: number[] = this.initialTower, finalTower: number[] = this.finalTower, auxiliarTower: number[] = this.auxiliarTower) => {
        if(numberOfDisks === 1) {
            this.makeMove(initialTower, finalTower);
            return;
        }
        this.solve(numberOfDisks-1, initialTower, auxiliarTower, finalTower);
        this.makeMove(initialTower, finalTower);
        this.solve(numberOfDisks-1, auxiliarTower, finalTower, initialTower);
    };

    private makeMove = (initialTower: number[], finalTower: number[]): void => {
        const topDisk = initialTower.pop();
        finalTower.push(topDisk);
        this.totalMoves++;
    };

    isValidTower = (tower:number[]): boolean => {
        if(tower.length!=this.numberOfDisks){
            return false;
        }
        let isOrdered = true;
        for(let i=1;i<tower.length;i++){
            if(tower[i-1]<tower[i]){
                isOrdered = false;
                break;
            }
        }
        return isOrdered;
    };

    private createDefaultTower = (): number[] => {
        const tower = [];
        let n = this.numberOfDisks;
        while (n > 0) {
            tower.push(n);
            n--;
        }
        return tower;
    };
}

// TEST
const test = ((n = 10) => {
    const hanoi = new Hanoi(n);
    hanoi.solve();
    console.log("Is final tower valid:", hanoi.isValidTower(hanoi.finalTower));
    console.log("Total moves made:", hanoi.totalMoves);
})()
