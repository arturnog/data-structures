import { isEqual, forEach } from 'lodash';

export interface TreeNode<T> {
    path: string;
    value: T | null;
    children: TreeNode<T>[];
};

// To be used for each node type
export interface Value {
  value: string;
  cost?: number;
};

export const initialTreeNode = (rootLevelPath?: string): TreeNode<Value> => {
  return {
    path: rootLevelPath || '',
    value: null,
    children: [],
  };
};

// Search in tree: return level where found, otherwise -1
// DFS using recursion
export const dfsSearchRecursive = (rootNode: TreeNode<Value>, value: Value, level = 0): number => {
  if (isEqual(rootNode.value, value)) {
    return level;
  }
  rootNode.children.forEach(nodeChild => {
    return dfsSearchRecursive(nodeChild, value, level+1);
  });
  return -1;
};

// DFS using iterative stack data structure
export const dfsSearchIterative = (rootNode: TreeNode<Value>, value: Value): number => {
  const stack: TreeNode<Value>[] = [];
  stack.push(rootNode);

  let level:number = 0;
  while(stack.length > 0){
    const node: TreeNode<Value> = stack.pop()!;

    if (isEqual(node.value, value)) {
      console.log("Found at level", level);
      return level;
    }

    rootNode.children.forEach(nodeChild => {
      stack.push(nodeChild);
    });
    level++;
  }
  return -1;
};

// BFS using queue data structure
export const bfsSearch = (rootNode: TreeNode<Value>, value: Value): number => {
  const queue: TreeNode<Value>[] = [];
  queue.push(rootNode);

  let level:number = 0;
  while(queue.length > 0){
    const node: TreeNode<Value> = queue.shift()!;

    if (isEqual(node.value, value)) {
      console.log("Found at level", level);
      return level;
    }

    rootNode.children.forEach(nodeChild => {
      queue.push(nodeChild);
    });
    level++;
  }
  return -1;
};

// Recursion approach
export const addToTreeRec = (rootNode: TreeNode<Value>, path: string, value: Value): TreeNode<Value> => {
    const pathLevels = path.split('/');
    if (pathLevels.length <= 1) {
      // at the correct tree level to add value
      rootNode.value = value;
      return rootNode;
    }
    const nextPathLevel = pathLevels.shift();
    const newPath = pathLevels.join('/');

    let newNode: TreeNode<Value>;
    let newNodePos: number;
    forEach(rootNode.children, (childNode, i) => {
      // determine which subtree to traverse
      if (childNode.path === nextPathLevel) {
        newNodePos = i;
        newNode = addToTreeRec(childNode, newPath, value);
        return false;
      }
      return null;
    });

    if (newNode && newNodePos) {
      // if there are is a path children, update the node
      rootNode.children[newNodePos] = newNode;
      return rootNode;
    }
    // if there are no children with that path, create one
    const newChildNode = initialTreeNode(nextPathLevel);

    const newSubTree = addToTreeRec(newChildNode, newPath, value);
    rootNode.children.push(newSubTree);
    return rootNode;
};

// Iterative approach using a stack data structure
export const addToTreeIter = (rootNode: TreeNode<Value>, path: string, value: Value): TreeNode<Value> => {
  const stack: TreeNode<Value>[] = [];
  stack.push(rootNode);

  while( stack.length > 0) {
    let node = stack.pop()!;

    const pathLevels = path.split('/');
    if (pathLevels.length <= 1) {
      node.value = value;
      break;
    }

    const nextPathLevel = pathLevels.shift();

    let hasProdigyChild = false;
    for (let i = 0; i < node.children.length; i++) {
      const childNode = node.children[i];
      
      if (childNode.path === nextPathLevel) {
        hasProdigyChild = true;
        path = pathLevels.join('/');
        stack.push(childNode);
        break;
      }
    }

    if(!hasProdigyChild) {
      const newChildNode = initialTreeNode(nextPathLevel);

      node.children.push(newChildNode);
      stack.push(node);
    }
  }
  return rootNode;
};

