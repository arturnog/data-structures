import { Lifo } from '../data_structures';

export class StackExamples {
    lifo: Lifo<number>;
    constructor() {
        this.lifo = new Lifo<number>();
    }

    isLifoSorted = (order: 'asc' | 'desc' = 'asc'): boolean => {
        const validateAsc = (value1: number, value2: number): boolean => {
            return value1<=value2;
        };
        const validateDesc = (value1: number, value2: number): boolean => {
            return value1>=value2;
        };
        const validateClb = order === 'asc' ? validateAsc : validateDesc;

        let cursor = this.lifo.rootNode;
        while (cursor != null) {
            if(cursor.next && !validateClb(cursor.value, cursor.next.value,)) {
                return false;
            }
            cursor = cursor.next;
        }
        return true;
    };

    reverseStack = (values: number[]) => {
        values.forEach(value => {
            this.lifo.push(value);
        });
    };
}

// TEST
(() => {
    const stackExamples = new StackExamples();
    const testArray = [5,4,7,2,1];
    stackExamples.reverseStack(testArray);
    const stackIsSorted = stackExamples.isLifoSorted();
    console.log("stackIsSorted", stackExamples.lifo.toArray(), stackIsSorted);
})();