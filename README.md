# Abstract Data Structures

Simple Typescript project which goal is to provide an implement of the following abstract Data Structures:

* Stack LIFO (Last-in First-out)
* Queue FIFO (First-in First-out)
* Tree

## Build project

1. npm install
2. npm run compile

## Run tests

1. npm run test

## Run a specific file

1. nodemon file.ts

